use crate::*;

pub fn handle_mouse_clicks(
    window: Res<Windows>,
    mouse_button_input: Res<Input<MouseButton>>,
    mut ev_cursor: EventReader<CursorMoved>,
    mut qs: QuerySet<(
        Query<&Transform, With<MainCamera>>,
        Query<(&mut Transform, &Unit)>,
    )>,
) {
    if let Some(pos) = ev_cursor.iter().last() {
        let camera_transform = qs.q0().iter().next().unwrap();
        let wnd = window.get(pos.id).unwrap();
        let size = Vec2::new(wnd.width() as f32, wnd.height() as f32);

        // the default orthographic projection is in pixels from the center;
        // just undo the translation
        let p = pos.position - size / 2.0;

        // apply the camera transform
        let mut pos_wld = camera_transform.compute_matrix() * p.extend(0.0).extend(1.0);

        pos_wld.z = 0.0;
        if mouse_button_input.pressed(MouseButton::Left) {
            for (mut t, _) in qs.q1_mut().iter_mut() {
                t.translation = pos_wld.into();
            }
        } else {
        }
    }
}
