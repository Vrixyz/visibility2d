use std::collections::HashMap;

use bevy::render::{
    mesh::Indices,
    pipeline::{PipelineDescriptor, PrimitiveTopology, RenderPipeline},
    shader::{ShaderStage, ShaderStages},
};
use bevy::{prelude::*, render::render_graph::base::MainPass};
use bevy_prototype_debug_lines::*;
use bevy_prototype_lyon::prelude::*;
mod input;

use input::*;

struct UnitMaterial {
    pub mat: Handle<ColorMaterial>,
}

pub struct MainCamera;
pub struct Unit;

struct Shape {
    pub verts: Vec<Vec2>,
}

fn main() {
    App::build()
        .insert_resource(ClearColor(Color::rgb(0., 0., 0.)))
        .add_plugins(DefaultPlugins)
        .add_plugin(ShapePlugin)
        .add_plugin(DebugLinesPlugin)
        .add_startup_system(init.system())
        .add_system(handle_mouse_clicks.system())
        //.add_system(debugShapes.system())
        //.add_system(debug_simple_shapes.system())
        .add_system(visibility.system())
        .run();
}

fn init(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut pipelines: ResMut<Assets<PipelineDescriptor>>,
    // Access to add new shaders
    mut shaders: ResMut<Assets<Shader>>,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    let texture_handle = asset_server.load("icon.png");
    let mat = materials.add(texture_handle.clone().into());
    commands.insert_resource(UnitMaterial { mat });
    commands
        .spawn_bundle(OrthographicCameraBundle::new_2d())
        .insert(MainCamera);

    let unit_entity = commands
        .spawn()
        .insert(Unit)
        .insert(Transform::from_translation(Vec3::new(0.0, 0.0, 0.0)))
        .insert(GlobalTransform::from_translation(Vec3::new(
            50.0, -50.0, 10.0,
        )))
        .id();
    commands
        .spawn_bundle(SpriteBundle {
            material: materials.add(texture_handle.into()),
            sprite: Sprite::new(Vec2::new(32.0, 32.0)),
            ..Default::default()
        })
        .insert(Parent(unit_entity))
        .id();
    let pipeline_visibility_handle =
        pipelines.add(PipelineDescriptor::default_config(ShaderStages {
            // Vertex shaders are run once for every vertex in the mesh.
            // Each vertex can have attributes associated to it (e.g. position,
            // color, texture mapping). The output of a shader is per-vertex.
            vertex: shaders.add(Shader::from_glsl(
                ShaderStage::Vertex,
                VERTEX_VISIBLE_SHADER,
            )),
            // Fragment shaders are run for each pixel belonging to a triangle on
            // the screen. Their output is per-pixel.
            fragment: Some(shaders.add(Shader::from_glsl(
                ShaderStage::Fragment,
                FRAGMENT_VISIBLE_SHADER,
            ))),
        }));
    let pipeline_shadow_handle = pipelines.add(PipelineDescriptor::default_config(ShaderStages {
        // Vertex shaders are run once for every vertex in the mesh.
        // Each vertex can have attributes associated to it (e.g. position,
        // color, texture mapping). The output of a shader is per-vertex.
        vertex: shaders.add(Shader::from_glsl(ShaderStage::Vertex, VERTEX_SHADER)),
        // Fragment shaders are run for each pixel belonging to a triangle on
        // the screen. Their output is per-pixel.
        fragment: Some(shaders.add(Shader::from_glsl(ShaderStage::Fragment, FRAGMENT_SHADER))),
    }));
    commands.insert_resource(ShadowMap {
        shadows: HashMap::new(),
        pipeline: pipeline_shadow_handle.clone(),
    });

    for i in 0..10 {
        let position = Vec2::splat((i as f32 - 5.0) * 50.0).extend(10.0);
        let obstacle_entity = commands
            .spawn()
            .insert(Transform::from_translation(position))
            .insert(Shape {
                verts: vec![
                    Vec2::new(0.0, 20.0),
                    Vec2::new(20.0, 0.0),
                    Vec2::new(-20.0, 0.0),
                ],
            })
            .id();
        let mut mesh = Mesh::new(bevy::render::pipeline::PrimitiveTopology::TriangleList);
        let v_pos = vec![[0.0, 20.0, 0.0], [20.0, 0.0, 0.0], [-20.0, 0.0, 0.0]];

        // Set the position attribute
        mesh.set_attribute(Mesh::ATTRIBUTE_POSITION, v_pos);
        let indices = vec![0, 2, 1];
        mesh.set_indices(Some(bevy::render::mesh::Indices::U32(indices)));

        // TODO: use shared mesh
        commands
            .spawn()
            .insert(Parent(obstacle_entity))
            .insert_bundle(MeshBundle {
                mesh: meshes.add(mesh),
                render_pipelines: RenderPipelines::from_pipelines(vec![RenderPipeline::new(
                    pipeline_shadow_handle.clone(),
                )]),
                global_transform: GlobalTransform::from_translation(position),
                ..Default::default()
            })
            .insert(Parent(obstacle_entity))
            .id();
    }

    let mut circle = Mesh::new(bevy::render::pipeline::PrimitiveTopology::TriangleList);
    let r = 100.0;
    let mut v_pos = vec![[0.0, 0.0, 0.0]];
    for i in 0..10 {
        let a = std::f32::consts::TAU / 4.0 - i as f32 * std::f32::consts::TAU / 10.0;
        // Add the vertex coordinates
        v_pos.push([r * a.cos(), r * a.sin(), 0.0]);
    }
    // Set the position attribute
    circle.set_attribute(Mesh::ATTRIBUTE_POSITION, v_pos);
    let mut indices = vec![0, 1, 10];
    for i in 2..=10 {
        indices.extend_from_slice(&[0, i, i - 1]);
    }
    circle.set_indices(Some(bevy::render::mesh::Indices::U32(indices)));

    let mut v_color = vec![[1.0, 1.0, 1.0]];
    v_color.extend_from_slice(&[[0.0, 0.0, 0.0]; 10]);
    circle.set_attribute("Vertex_Color", v_color);
    commands
        .spawn_bundle(MeshBundle {
            mesh: meshes.add(circle),
            render_pipelines: RenderPipelines::from_pipelines(vec![RenderPipeline::new(
                pipeline_visibility_handle,
            )]),
            ..Default::default()
        })
        .insert(Parent(unit_entity))
        .id();
}
const VERTEX_VISIBLE_SHADER: &str = r"
#version 450
layout(location = 0) in vec3 Vertex_Position;
layout(location = 1) in vec3 Vertex_Color;
layout(location = 1) out vec3 v_Color;
layout(set = 0, binding = 0) uniform CameraViewProj {
    mat4 ViewProj;
};
layout(set = 1, binding = 0) uniform Transform {
    mat4 Model;
};
void main() {
    v_Color = Vertex_Color;
    gl_Position = ViewProj * Model * vec4(Vertex_Position, 1.0);
}
";

const FRAGMENT_VISIBLE_SHADER: &str = r"
#version 450
layout(location = 1) in vec3 v_Color;
layout(location = 0) out vec4 o_Target;
void main() {
    o_Target = vec4(v_Color, 1.0);
}
";

const VERTEX_SHADER: &str = r"
#version 450


layout(location = 0) in vec3 Vertex_Position;
layout(set = 0, binding = 0) uniform CameraViewProj {
    mat4 ViewProj;
};
layout(set = 1, binding = 0) uniform Transform {
    mat4 Model;
};
void main() {
    gl_Position = ViewProj * Model * vec4(Vertex_Position, 1.0);
}
";

const FRAGMENT_SHADER: &str = r"
#version 450

layout(location = 0) out vec4 o_Target;
void main() {
    o_Target = vec4(0.0, 0.0, 0.0, 1.0);
}
";
fn debug_simple_shapes(mut lines: ResMut<DebugLines>, q_shapes: Query<(&Transform, &Shape)>) {
    for (t, s) in q_shapes.iter() {
        let mut start: Option<Vec2> = None;
        for v in s.verts.iter() {
            if let Some(first) = start {
                lines.line_colored(
                    t.translation + first.extend(1.0),
                    t.translation + v.extend(1.0),
                    0.0,
                    Color::GREEN,
                );
            }

            start = Some(Vec2::new(v[0], v[1]));
        }
        if let Some(last) = start {
            if let Some(first) = s.verts.first() {
                lines.line_colored(
                    t.translation + first.extend(1.0),
                    t.translation + last.extend(1.0),
                    0.0,
                    Color::GREEN,
                );
            }
        }
    }
}

fn shadow_point(origin: &Vec2, shadow_start: &Vec2, shadow_end: f32) -> Vec2 {
    let offset = *shadow_start - *origin;
    let distance = offset.length();
    *shadow_start + offset.normalize() * (shadow_end - distance)
}

struct ShadowMap {
    /// shadow's key is entities of (unit, shape)
    pub shadows: HashMap<(Entity, Entity), Handle<Mesh>>,
    pub pipeline: Handle<PipelineDescriptor>,
}

fn visibility(
    mut commands: Commands,
    q: QuerySet<(
        Query<(Entity, &Transform, &Unit)>,
        Query<(Entity, &Transform, &Shape)>,
    )>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut shadow_map: ResMut<ShadowMap>,
    mut lines: ResMut<DebugLines>,
) {
    for (eu, tu, _u) in q.q0().iter() {
        for (es, ts, s) in q.q1().iter() {
            let mut angles_verts = vec![];
            let dir_s_to_u: Vec2 = (tu.translation - ts.translation).into();
            let dir_s_to_u = dir_s_to_u.normalize();
            for v in s.verts.iter() {
                let world_pos = Vec2::from(ts.translation) + *v;
                let u_to_v: Vec2 = world_pos - tu.translation.into();
                let u_to_v = u_to_v.normalize();
                let mut angle = dir_s_to_u.y.atan2(dir_s_to_u.x) - u_to_v.y.atan2(u_to_v.x);
                if angle < 0.0 {
                    angle += 2.0 * std::f32::consts::PI;
                }

                angles_verts.push((angle, world_pos));
            }
            angles_verts.sort_by(|av, av2| {
                av.0.partial_cmp(&av2.0)
                    .unwrap_or(std::cmp::Ordering::Equal)
            });
            let mut shadow_pos = vec![];
            assert!(
                angles_verts.len() > 1,
                "less than 2 vertices is impossible to compute a shadow."
            );
            let shadow_length: f32 = 1200.0;
            let first = angles_verts.first().unwrap();
            let last = angles_verts.last().unwrap();
            let middle = (last.1 + first.1) / 2.0;
            let origin: Vec2 = tu.translation.into();
            shadow_pos.push(first.1);
            shadow_pos.push(shadow_point(&origin, &first.1, shadow_length));
            shadow_pos.push(shadow_point(&origin, &middle, shadow_length));
            shadow_pos.push(shadow_point(&origin, &last.1, shadow_length));
            shadow_pos.push(last.1);
            /*for i in 1..shadow_pos.len() {
                let prev_point = shadow_pos[i - 1];
                let cur_point = shadow_pos[i % shadow_pos.len()];
                lines.line_colored(
                    prev_point.extend(1.0),
                    cur_point.extend(1.0),
                    0.0,
                    Color::RED,
                );
            }*/

            if let Some(shadow) = shadow_map.shadows.get(&(eu, es)) {
                if let Some(mesh) = meshes.get_mut(shadow) {
                    let pos: Vec<[f32; 3]> = shadow_pos.iter().map(|v| [v.x, v.y, 0.0]).collect();
                    mesh.set_attribute(Mesh::ATTRIBUTE_POSITION, pos);
                    mesh.set_indices(Some(Indices::U32(vec![0, 2, 1, 0, 4, 2, 4, 3, 2])));
                }
            } else {
                let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
                let pos: Vec<[f32; 3]> = shadow_pos.iter().map(|v| [v.x, v.y, 0.0]).collect();
                mesh.set_attribute(Mesh::ATTRIBUTE_POSITION, pos);
                mesh.set_indices(Some(Indices::U32(vec![0, 2, 1, 0, 4, 2, 4, 3, 2])));
                let mesh_handle = meshes.add(mesh);
                shadow_map.shadows.insert((eu, es), mesh_handle.clone());
                commands.spawn_bundle(MeshBundle {
                    mesh: mesh_handle,
                    render_pipelines: RenderPipelines::from_pipelines(vec![RenderPipeline::new(
                        shadow_map.pipeline.clone(),
                    )]),
                    transform: Transform::from_translation(Vec3::new(0.0, 0.0, 100.0)),
                    ..Default::default()
                });
            }
        }
    }
}

pub fn debug_shapes(
    meshes: ResMut<Assets<Mesh>>,
    mut lines: ResMut<DebugLines>,
    q_mesh: Query<(Entity, &Handle<Mesh>)>,
) {
    for (e, m_handle) in q_mesh.iter() {
        let m = if let Some(m) = meshes.get(m_handle.clone()) {
            m
        } else {
            continue;
        };
        let mut start: Option<Vec2> = None;
        // See https://docs.rs/bevy/0.5.0/bevy/render/mesh/struct.Mesh.html
        // Easiest way would be to create an easier shape to read (see Shape at top of file)
        // then read its data.
        // once I have that data, do math to compute visibility from units https://www.redblobgames.com/articles/visibility/

        let vert_positions = m.attribute(Mesh::ATTRIBUTE_POSITION);
        let vert_positions = match vert_positions {
            Some(r) => match r {
                bevy::render::mesh::VertexAttributeValues::Float3(verts) => {
                    println!("Float3");
                    verts
                }
                _ => {
                    println!("not supported");
                    continue;
                }
            },
            None => {
                continue;
            }
        };
        let indices = match m.indices() {
            Some(i) => i,
            None => {
                println!("{:#?}: no indices ^^", e);
                continue;
            }
        };
        match indices {
            Indices::U16(_vert_indices) => {
                println!("U16 indices not supported");
            }
            Indices::U32(vert_indices) => {
                println!("U32");
                for v_id in vert_indices.iter() {
                    let v = vert_positions[*v_id as usize];
                    if let Some(first) = start {
                        let mut pos: Vec3 = v.into();
                        pos.x *= 20.0;
                        pos.y *= 20.0;
                        lines.line_colored(first.extend(1.0), pos, 0.0, Color::GREEN);
                    }

                    start = Some(Vec2::new(v[0] * 20.0, v[1] * 20.0));
                }
            }
        }
    }
}
